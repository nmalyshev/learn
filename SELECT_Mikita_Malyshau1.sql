-- 1 task
-- 1 solution
SELECT 
   staff.staff_id,
   staff.first_name,
   staff.last_name,
   a.amount
 FROM staff
 LEFT JOIN (
   SELECT
     staff_id,
     SUM(amount) AS amount
   FROM payment
  WHERE date_part('year', payment.payment_date) = '2017'
  --WHERE EXTRACT(YEAR FROM payment_date) = '2017'
   GROUP BY staff_id
  ORDER BY amount DESC
 ) AS a
 ON staff.staff_id = a.staff_id;

-- 2 solution
 SELECT
  s.staff_id,
   s.first_name,
   s.last_name,
   COALESCE(SUM(p.amount), 0) AS amount
 FROM staff s
 LEFT JOIN payment p ON s.staff_id = p.staff_id AND EXTRACT(YEAR FROM p.payment_date) = 2017
 GROUP BY s.staff_id, s.first_name, s.last_name
 ORDER BY amount DESC;


-- 2 task
-- 1 solution
 SELECT 
   film.film_id,
   b.film_count,
   film.rating
 FROM film
 INNER JOIN (
   SELECT
     inventory.film_id,
     SUM(a.cnt) as film_count
  FROM inventory
   INNER JOIN (
     SELECT
       inventory_id,
       COUNT(inventory_id) as cnt
     FROM rental
     GROUP BY inventory_id
   ) as a
   ON inventory.inventory_id = a.inventory_id
   GROUP BY film_id
 ) as b
 ON film.film_id = b.film_id
ORDER BY b.film_count DESC;

-- 2 solution
SELECT
 film.film_id,
  COALESCE(rental_counts.film_count, 0) AS film_count,
  film.rating
 FROM film
 LEFT JOIN (
   SELECT
     i.film_id,
     COUNT(r.inventory_id) AS film_count
   FROM inventory i
   LEFT JOIN rental r ON i.inventory_id = r.inventory_id
   GROUP BY i.film_id
 ) AS rental_counts
 ON film.film_id = rental_counts.film_id
 ORDER BY film_count DESC;


-- 3 task 
-- 1 solution
select max(release_year), actor.actor_id
from film
inner join (
	select actor_id, film_id
 	from film_actor
 ) as actor
 on film.film_id = actor.film_id
group by actor.actor_id
 order by 1;

-- 2 solution
SELECT
MAX(film.release_year) AS max_release_year,
film_actor.actor_id
FROM film_actor
INNER JOIN film ON film_actor.film_id = film.film_id
GROUP BY film_actor.actor_id
ORDER BY max_release_year DESC;





