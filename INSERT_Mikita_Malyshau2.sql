--1 
INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update, special_features, fulltext)
VALUES ('Barbie', 'A visually dazzling comedy about a doll living in Barbieland', 2023, 1, null, 7, 3.99, 120, 19.99, 'PG-13', CURRENT_TIMESTAMP, '{"Special Features Here"}', 'Full Text Here');
--2 
INSERT INTO actor (first_name, last_name, last_update)
VALUES
('Margot','Robbie', CURRENT_TIMESTAMP),
('Ryan', 'Gosling', CURRENT_TIMESTAMP),
('Issa', 'Rae', CURRENT_TIMESTAMP),
('Kate', 'McKinnon', CURRENT_TIMESTAMP);
INSERT INTO film_actor (actor_id, film_id, last_update)
VALUES
((SELECT actor_id FROM actor WHERE first_name = 'Margot' ), (SELECT film_id FROM film WHERE title = 'Barbie'), CURRENT_TIMESTAMP),
((SELECT actor_id FROM actor WHERE first_name = 'Ryan' ), (SELECT film_id FROM film WHERE title = 'Barbie'), CURRENT_TIMESTAMP),
((SELECT actor_id FROM actor WHERE first_name = 'Issa' ), (SELECT film_id FROM film WHERE title = 'Barbie'), CURRENT_TIMESTAMP),
((SELECT actor_id FROM actor WHERE first_name = 'Kate' ), (SELECT film_id FROM film WHERE title = 'Barbie'), CURRENT_TIMESTAMP);
--3 
INSERT INTO inventory (film_id, store_id , last_update)
VALUES
((SELECT film_id FROM film WHERE title = 'Barbie' LIMIT 1 ), (SELECT store_id FROM store WHERE store_id IN (1, 2) LIMIT 1 ), CURRENT_TIMESTAMP);
